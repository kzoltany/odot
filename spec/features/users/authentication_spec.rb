require "spec_helper"

describe "Logging In" do
  it "logs the user in and goes to the todo lists" do
    User.create(first_name: "Kevin", last_name: "Zoltany", email: "kevinyon100@gmail.com", password: "farmville12", password_confirmation: "farmville12")
    visit "/"
    click_link "Sign In"
    fill_in "Email Address", with: "kevinyon100@gmail.com"
    fill_in "Password", with: "farmville12"
    click_button "Sign In"
    
    expect(page).to have_content("Todo Lists")
    expect(page).to have_content("Thanks for logging in!")
  end

  it "diplays the email address in the event of a failed login" do
    visit new_user_session_path
    fill_in "Email Address", with: "kevinyon100@gmail.com"
    fill_in "Password", with: "incorrect"
    click_button "Sign In"

    expect(page).to have_content("Please check your email and password")
    expect(page).to have_field("Email Address", with: "kevinyon100@gmail.com")
  end
end